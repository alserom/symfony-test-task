<?php

namespace Romanov\ContactFormBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Romanov\ContactFormBundle\Entity\ContactMessage;

/**
 * Class ContactSendCommand
 *
 * Обработка консольной команды для отправки очереди сообщений
 *
 * @package Romanov\ContactFormBundle\Command
 */
class ContactSendCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('crontask:contact')->setDescription('Send messages from contact form');
    }

    /**
     * Обработка и отправка сообщений
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $crontasks = $em->getRepository('ContactFormBundle:ContactMessage')->findAll();
        $to_email = $this->getContainer()->getParameter('cf_to_email');
        $from_email_tmp = $this->getContainer()->getParameter('cf_from_email');
        foreach($crontasks as $msg){
            $from_email = filter_var($from_email_tmp, FILTER_VALIDATE_EMAIL) ? $from_email_tmp : $msg->getEmail();
            $next = $msg->getNextTryTime();
            if($next <= time()){
                $mailer = $this->getContainer()->get('mailer');
                $message = \Swift_Message::newInstance()
                    ->setSubject($msg->getSubject())
                    ->setFrom($from_email)
                    ->setTo($to_email)
                    ->setBody($this->getContainer()->get('templating')->render('ContactFormBundle:Mail:contact.html.twig', [
                        'ip' => $msg->getIp(),
                        'name' => $msg->getName(),
                        'email' => $msg->getEmail(),
                        'message' => $msg->getMessage(),
                        'dt' => $msg->getDate()
                    ]), 'text/html');
                try{
                    $mailer->send($message);
                }catch (\Exception $e){
                    $plus_time = $this->getContainer()->getParameter('cf_fail_delay');
                    $msg->setErrorInfo($e->getMessage());
                    $utime = time() + intval($plus_time);
                    $msg->setNextTryTime($utime);
                    continue;
                }
                $em->remove($msg);
            }
        }
        $em->flush();
    }
}