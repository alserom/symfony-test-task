<?php

namespace Romanov\ContactFormBundle\Entity;

/**
 * Class ContactMessage
 *
 * Описание обьекта "сообщение"
 *
 * @package Romanov\ContactFormBundle\Entity
 */
class ContactMessage
{
    protected $id;
    protected $name;
    protected $email;
    protected $subject;
    protected $message;
    protected $ip;
    protected $date;
    protected $error_info;
    protected $next_try_time;

    /**
     * Получение поля id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Установка поля name
     *
     * @param string $name
     * @return ContactMessage
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Получение поля name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Установка поля email
     *
     * @param string $email
     * @return ContactMessage
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Получение поля email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Установка поля subject
     *
     * @param string $subject
     * @return ContactMessage
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Получение поля subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Установка поля message
     *
     * @param string $message
     * @return ContactMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Получение поля message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Установка поля ip
     *
     * @param string $ip
     * @return ContactMessage
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * Получение поля ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Установка поля error_info
     *
     * @param string $errorInfo - сообщение о неудачной отправке
     * @return ContactMessage
     */
    public function setErrorInfo($errorInfo)
    {
        $this->error_info = $errorInfo;
        return $this;
    }

    /**
     * Получение поля error_info
     *
     * @return string 
     */
    public function getErrorInfo()
    {
        return $this->error_info;
    }

    /**
     * Установка поля next_try_time
     *
     * @param integer $nextTryTime - время следующей попытки отправки в timestamp
     * @return ContactMessage
     */
    public function setNextTryTime($nextTryTime)
    {
        $this->next_try_time = $nextTryTime;
        return $this;
    }

    /**
     * Получение поля next_try_time
     *
     * @return integer 
     */
    public function getNextTryTime()
    {
        return $this->next_try_time;
    }

    /**
     * Установка поля date
     *
     * @param integer $date - время отправки формы в timestamp
     * @return ContactMessage
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Получение поля date
     *
     * @return integer 
     */
    public function getDate()
    {
        return $this->date;
    }
}
