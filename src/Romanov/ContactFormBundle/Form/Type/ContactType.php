<?php

namespace Romanov\ContactFormBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

class ContactType extends AbstractType
{
    /**
     * Построение формы
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', [
            'attr' => [
                'placeholder' => 'Enter your name',
                'required' => true
            ]
        ]);
        $builder->add('email', 'email', [
            'attr' => [
                'placeholder' => 'Enter your email',
                'required' => true
            ]
        ]);
        $builder->add('subject', 'text', [
            'attr' => [
                'placeholder' => 'Subject of your message',
            ]
        ]);
        $builder->add('message', 'textarea', [
            'attr' => [
                'rows' => 6,
                'placeholder' => 'Enter your message',
                'required' => true
            ]
        ]);
    }

    /**
     * Установка опций и валидация формы
     *
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $collectionConstraint = new Collection(array(
            'name' => array(
                new NotBlank(array('message' => 'Name field is required!')),
                new Length(array('min' => 2, 'max' => 100))
            ),
            'email' => array(
                new NotBlank(array('message' => 'Email field is required!')),
                new Email(array('message' => 'Invalid email address.')),
                new Length(array('max' => 100))
            ),
            'subject' => array(
                new Length(array('max' => 100))
            ),
            'message' => array(
                new NotBlank(array('message' => 'Message should not be blank.')),
                new Length(array('min' => 5))
            )
        ));

        $resolver->setDefaults(array(
            'constraints' => $collectionConstraint,
            'required' => false
        ));
    }

    /**
     * @return string - имя формы
     */
    public function getName()
    {
        return 'contact';
    }
}