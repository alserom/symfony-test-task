<?php

namespace Romanov\ContactFormBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Romanov\ContactFormBundle\Entity\ContactMessage;
use Romanov\ContactFormBundle\Form\Type\ContactType;

/**
 * Класс для работы с контактной формой
 *
 * @package Romanov\ContactFormBundle\Controller
 */
class ContactController extends Controller
{
    /**
     * Вывод отдельной страницы с контактной формой
     *
     * @return Response
     */
    public function indexAction()
    {
        $form = $this->createForm(new ContactType());
        return $this->render('ContactFormBundle:Default:index.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Метод обработки контактной формы, посланной с отдельной страницы
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function sendAction(Request $request)
    {
        $form = $this->createForm(new ContactType());
        $form->bind($request);
        if ($form->isValid())
        {
            $cm = new ContactMessage();
            $cm->setName($form->get('name')->getData());
            $cm->setEmail($form->get('email')->getData());
            $cm->setSubject($form->get('subject')->getData());
            $cm->setMessage($form->get('message')->getData());
            $cm->setIp($request->getClientIp());
            $cm->setNextTryTime(0);
            $cm->setDate(time());

            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($cm);
            $em->flush();

            $this->addFlash('success_send','Your message successfully sent.');
            return $this->redirectToRoute('contact_form_homepage');
        }
        return $this->render('ContactFormBundle:Default:index.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Метод сохранения сообщения в очередь (при использовании виджета контактной формы)
     *
     * @param Request $request
     * @param \Symfony\Component\Form\Form $form
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function saveMessage($request, $form, $em)
    {
        $cm = new ContactMessage();
        $cm->setName($form->get('name')->getData());
        $cm->setEmail($form->get('email')->getData());
        $cm->setSubject($form->get('subject')->getData());
        $cm->setMessage($form->get('message')->getData());
        $cm->setIp($request->getClientIp());
        $cm->setNextTryTime(0);
        $cm->setDate(time());
        $em->persist($cm);
        $em->flush();
    }

    /**
     * Метод удаления сообщения из очереди
     *
     * @param $id - идентификатор сообщения
     * @param \Doctrine\ORM\EntityManager $em
     * @return Response
     */
    public function removeMsgFromQueue($id, $em)
    {
        $message = $em->getRepository('ContactFormBundle:ContactMessage')->find($id);
        $res['success'] = false;
        if($message && $message->getErrorInfo()){
            $em->remove($message);
            $em->flush();
            $res['success'] = true;
            $res['id'] = $id;
            $res['usermail'] = $message->getEmail();
        }
        return new Response(json_encode($res));
    }
}