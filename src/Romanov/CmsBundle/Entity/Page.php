<?php

namespace Romanov\CmsBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Page
 *
 * Описание обьекта "страница"
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="pages")
 * @UniqueEntity("name")
 * @UniqueEntity(fields="path", message="This slug is already in use with that parent page.")
 * @UniqueEntity(fields={"slug", "parent_id"}, errorPath="slug", message="This slug is already in use with that parent page.")
 */
class Page
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $parent_id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @Assert\NotBlank(message="Name field is required!")
     * @Assert\Length(min=2, max=50)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Slug field is required!")
     * @Assert\Length(max=50)
     * @Assert\Regex(pattern="/^[a-z0-9-]+$/", message="Slug must be a correct (regex /^[a-z0-9-]+$/).")
     */
    protected $slug;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    protected $path;

    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\NotBlank(message="Title field is required!")
     * @Assert\Length(max=200)
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Content field is required!")
     */
    protected $content;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="parent")
     **/
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     **/
    protected $parent;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Обновление временных меток страницы при создании или обновлении
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateTimestamps()
    {
        $this->setUpdatedAt(new \DateTime());
        if($this->getCreatedAt() == null)
        {
            $this->setCreatedAt(new \DateTime());
        }
    }

    /**
     * Получение поля id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Установка поля parent_id
     *
     * @param integer $parentId
     * @return Page
     */
    public function setParentId($parentId)
    {
        $this->parent_id = $parentId;
        return $this;
    }

    /**
     * Получение поля parent_id
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Установка поля name
     *
     * @param string $name
     * @return Page
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Получение поля name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Установка поля slug
     *
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * Получение поля slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Установка поля path
     *
     * @param string $path
     * @return Page
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Получение поля path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Установка поля title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Получение поля title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Установка поля content
     *
     * @param string $content
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * Получение поля content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Установка даты создания страницы
     *
     * @param \DateTime $createdAt
     * @return Page
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
        return $this;
    }

    /**
     * Получение даты создания страницы
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Установка даты обновления страницы
     *
     * @param \DateTime $updatedAt
     * @return Page
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
        return $this;
    }

    /**
     * Получение даты обновления страницы
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }


    /**
     * Добавление дочерней страницы
     *
     * @param \Romanov\CmsBundle\Entity\Page $children
     * @return Page
     */
    public function addChild(\Romanov\CmsBundle\Entity\Page $children)
    {
        $this->children[] = $children;
        return $this;
    }

    /**
     * Удаление страницы из коллекции дочерних
     *
     * @param \Romanov\CmsBundle\Entity\Page $children
     */
    public function removeChild(\Romanov\CmsBundle\Entity\Page $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Получение дочерних страниц
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Установка родителя
     *
     * @param \Romanov\CmsBundle\Entity\Page $parent
     * @return Page
     */
    public function setParent(\Romanov\CmsBundle\Entity\Page $parent = null)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Получение родителя
     *
     * @return \Romanov\CmsBundle\Entity\Page 
     */
    public function getParent()
    {
        return $this->parent;
    }


    /**
     * Проверка, является ли страница корневой
     *
     * @return bool
     */
    public function isRoot()
    {
        return ($this->parent_id == null);
    }

    /**
     * Проверка, имеет ли страница дочерние страницы
     *
     * @return bool
     */
    public function hasChild()
    {
        return !$this->children->isEmpty();
    }

    /**
     * Метод получения имени страницы для выпадающего списка
     *
     * Используется, чтобы наглядно отделить родительские страницы от дочерних
     *
     * @return string
     */
    public function getIndentedName()
    {
        $level = 0;
        $p = $this->getParent();
        while ($p instanceof \Romanov\CmsBundle\Entity\Page) {
            $level++;
            $p = $p->getParent();
        }
        return sprintf(
            '%s %s',
            str_repeat('--', $level),
            $this->getName()
        );
    }
}
