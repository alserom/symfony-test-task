<?php

namespace Romanov\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;

class PageType extends AbstractType
{
    /**
     * Построение формы
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('parent', 'entity', array(
            'class' => 'CmsBundle:Page',
            'choice_label' => 'indentedName',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')
                    ->orderBy('p.path', 'ASC')
                    ->addOrderBy('p.parent_id', 'ASC');
            },
            'label' => 'Choose parent page',
            'placeholder' => 'Create as parent page',
            'required' => false,
        ));
        $builder->add('name', 'text', [
            'attr' => [
                'placeholder' => 'Enter page unique name',
            ]
        ]);
        $builder->add('slug', 'text', [
            'attr' => [
                'placeholder' => 'Enter page slug',
            ]
        ]);
        $builder->add('title', 'text', [
            'attr' => [
                'placeholder' => 'Enter page title',
            ]
        ]);
        $builder->add('content', 'textarea', [
            'attr' => [
                'rows' => 10,
            ]
        ]);
    }

    /**
     * @return string - имя формы
     */
    public function getName()
    {
        return 'page';
    }
}