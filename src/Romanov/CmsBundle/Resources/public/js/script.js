// Используем JS компонент bootstrap для показа атрибута title
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

// Используем jquery плагин treegrid для построения таблицы с элементами в виде дерева
$(function() {
    $('.tree').treegrid({
        expanderExpandedClass: 'glyphicon glyphicon-minus',
        expanderCollapsedClass: 'glyphicon glyphicon-plus',
        initialState: 'collapsed'
    });
});

// Используем WYSIWYG редактор
$(function() {
    CKEDITOR.replace( 'page_content' );
});

// Удаление сообщения из очереди посредством отправки ajax запроса
function delMsgFromQueue(msgId){
    swal({
        title: "Are you sure?",
        text: "Message with id "+msgId+" will be deleted from queue.",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete',
        closeOnConfirm: false
    }, function(){
        swal.disableButtons();
        $.ajax({
            url: '/admin',
            type: 'DELETE',
            data: {id: msgId}
        })
        .done(function(response) {
            var res = $.parseJSON(response);
            if(res.success){
                swal('Success!', 'A message with id '+res.id+' of '+res.usermail+' removed', 'success' );
                var newcount = $('#mcount').text() - 1;
                $('#mcount').text(newcount);
                $('#msg-'+res.id).hide('slow');
            }else{
                swal('Error!', 'Message cannot be deleted', 'error' );
            }
        })
        .fail(function() {
            swal('Ajax error', '', 'error' );
        });
    });
}

// Удаление страницы CMS посредством отправки ajax запроса
function delPage(pageId){
    swal({
        title: "Are you sure?",
        text: "Page with id "+pageId+" will be deleted with all child pages!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Delete',
        closeOnConfirm: false
    }, function(){
        swal.disableButtons();
        $.ajax({
            url: '/admin/pages/update/'+pageId,
            type: 'DELETE',
            data: {id: pageId}
        })
        .done(function(response) {
            var res = $.parseJSON(response);
            if(res.success){
                swal({
                    title: 'Success!',
                    html: 'A page with id '+res.id+' deleted<hr>Retirecting to page list...',
                    type: 'success',
                    showConfirmButton: false
                });
                setTimeout(function(){
                    window.location.href = '/admin/pages';
                }, 2500);
            }else{
                swal('Error!', 'Page cannot be deleted', 'error' );
            }
        })
        .fail(function() {
            swal('Ajax error', '', 'error' );
        });
    });
}