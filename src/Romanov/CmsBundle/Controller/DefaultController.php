<?php

namespace Romanov\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Romanov\CmsBundle\Entity\Page;
use Romanov\CmsBundle\Form\Type\PageType;

use Romanov\ContactFormBundle\Form\Type\ContactType;

/**
 * Основной контроллер для работы с CMS бандлом
 *
 * @package Romanov\CmsBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * Метод обработки запросов к обычным страницам CMS
     *
     * @param Request $request
     * @param string $path - URL путь к странице
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(Request $request, $path)
    {
        $em = $this->getDoctrine()->getEntityManager();
        if($path == null){
            $page = $em->getRepository('CmsBundle:Page')->findOneByName('main');
        }else{
            $path = '/'.$path;
            $page = $em->getRepository('CmsBundle:Page')->findOneByPath($path);
            if(!$page){
                throw $this->createNotFoundException('Page not found');
            }
        }
        $data['page'] = $page;
        $data['menu'] = $this->getMenu();

        /**
         * Проверка, нужно ли вставлять в страницу контактную форму
         * @todo Реализовать виджет контактной формы правильным способом
         */
        if($page && strpos($page->getContent(), '%contact_form%')){
            $data['with_contact_form'] = true;
            $form = $this->createForm(new ContactType());
            if($request->isMethod('POST') && $request->request->has($form->getName())){
                $form->bind($request);
                if ($form->isValid())
                {
                    $em = $this->getDoctrine()->getEntityManager();
                    $this->get('contact_form')->saveMessage($request, $form, $em);
                    $this->addFlash('success_send','Your message successfully sent.');
                    return $this->redirect($request->headers->get('referer'));
                }
            }
            $data['form'] = $form->createView();
        }
        return $this->render('CmsBundle:Default:index.html.twig', $data);
    }

    /**
     * Обработка главной страницы админ панели
     * + обработка запроса на удаление сообщения из очереди
     *
     * @param Request $request
     * @return Response
     */
    public function adminAction(Request $request)
    {
        if($request->isXmlHttpRequest() && $request->isMethod('DELETE')){
            $id = $request->get('id');
            $em = $this->getDoctrine()->getEntityManager();
            return $this->get('contact_form')->removeMsgFromQueue($id, $em);
        }

        $data['messages'] = $this->getDoctrine()->getRepository('ContactFormBundle:ContactMessage')->findAll();
        return $this->render('CmsBundle:Admin:contact_queue.html.twig', $data);
    }

    /**
     * Обработка страницы админ панели со списком всех страниц CMS
     *
     * @return Response
     */
    public function listPagesAction()
    {
        $repository = $this->getDoctrine()->getRepository('CmsBundle:Page');
        $query = $repository->createQueryBuilder('p')
            ->where('p.parent_id is NULL')
            ->orderBy('p.path', 'ASC')
            ->getQuery();
        $data['pages'] = $query->getResult();
        return $this->render('CmsBundle:Admin:page_list.html.twig', $data);
    }

    /**
     * Создание страницы CMS
     *
     * Вывод формы на добавление страницы и ее обработка
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createPageAction(Request $request)
    {
        $page = new Page();
        $form = $this->createForm(new PageType(), $page);
        if($request->isMethod('POST'))
        {
            $form->bind($request);
            if ($form->isValid())
            {
                $parent = $form->get('parent')->getData();
                if($parent instanceof Page){
                    $path = $parent->getPath()."/".$form->get('slug')->getData();
                }else{
                    $path = '/'.$form->get('slug')->getData();
                    $parent = null;
                }
                if($form->get('name')->getData() == 'main')
                {
                    $parent = null;
                    $path = null;
                }

                $page = $form->getData();
                $page->setPath($path);
                $page->setParent($parent);

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($page);
                $em->flush();

                $this->addFlash('success_create_page','Page successfully created.');
                return $this->redirectToRoute('cms_pages');
            }
        }
        $data['form'] = $form->createView();
        $data['action'] = 'create';
        return $this->render('CmsBundle:Admin:page_manage.html.twig', $data);
    }

    /**
     * Обновление страницы CMS
     *
     * Вывод формы и ее обработка + обработка запроса на удаление страницы
     *
     * @param Request $request
     * @param integer $id - идентификатор страницы
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function updatePageAction(Request $request, $id)
    {
        if($request->isXmlHttpRequest() && $request->isMethod('DELETE')){
            $id = $request->get('id');
            return $this->deletePage($id);
        }
        $em = $this->getDoctrine()->getEntityManager();
        $page = $em->getRepository('CmsBundle:Page')->find($id);
        if (!$page) {
            throw $this->createNotFoundException('No page found for id '.$id);
        }
        $parent_back = $page->getParent();

        $form = $this->createForm(new PageType(), $page);
        if($request->isMethod('POST'))
        {
            $form->bind($request);
            if ($form->isValid())
            {

                if($page->getParent()){
                    $path = $page->getParent()->getPath()."/".$page->getSlug();
                }else{
                    $path = "/".$page->getSlug();
                }
                if($page->getName() == 'main'){
                    $path = null;
                    $page->setParent(null);
                }

                $page->setPath($path);

                $validator = $this->get('validator');
                $errors = $validator->validate($page);
                if (count($errors) > 0) {
                    $form->get('slug')->addError(new FormError($errors[0]->getMessage()));
                }elseif(($page->getParent() instanceof Page) &&  $page->getId() == $page->getParent()->getId()){
                    $page->setParent($parent_back);
                    $form->get('parent')->addError(new FormError('Parent page cannot matched with self!'));
                }else{
                    $em->flush();
                    $this->addFlash('success_create_page','Page successfully updated.');
                    return $this->redirectToRoute('cms_pages');
                }
            }
        }

        $data['form'] = $form->createView();
        $data['action'] = 'update';
        return $this->render('CmsBundle:Admin:page_manage.html.twig', $data);
    }

    /**
     * Метод удаления страницы CMS
     *
     * @param $id - идентификатор страницы
     * @return Response
     */
    private function deletePage($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $page = $em->getRepository('CmsBundle:Page')->find($id);
        $res['success'] = false;
        if($page){
            $em->remove($page);
            $em->flush();
            $res['success'] = true;
            $res['id'] = $id;
        }
        return new Response(json_encode($res));
    }

    /**
     * Метод получения страниц CMS в виде списка меню
     * Возвращает строку в html формате
     *
     * @return string
     */
    private function getMenu()
    {
        $repository = $this->getDoctrine()->getRepository('CmsBundle:Page');
        $query = $repository->createQueryBuilder('p')
            ->orderBy('p.path', 'ASC')
            ->addOrderBy('p.parent_id', 'ASC')
            ->getQuery();
        $data['pages'] = $query->getResult();
        return $this->get('templating')->render('CmsBundle:Default:menu.html.twig', $data);
    }
}
